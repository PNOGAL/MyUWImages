#!/bin/bash
if [ "$#" = "0" ]; then
 mvn -Djava.awt.headless=true clean install
else 
 mvn -Djava.awt.headless=true $@
fi

mvn -Djava.awt.headless=true tomcat7:redeploy

unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
 notify-send "build complete for MyUW Images" 
elif [[ "$unamestr" == 'Darwin' ]]; then
 osascript -e 'display notification "MyUW Images build.sh finished" with title "MyUW Images deployed" sound name "Hero"'
fi
